// pages/play/play.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 传递过来音乐id
    musicId: '',
    // 播放状态
    action: {
      "method": "play"
    },
    // 歌曲名字
    name: '',
    // 歌曲图片
    imgUrl: '',
    // 时间歌词列表
    lycTimeList: [],
    // 当前播放歌词的index
    index: -1,
    // 是否有歌词
    hasLyc: false,
    // 歌词滚动条距离顶部的距离
    top: 0,
    // 播放模式
    mode: "loop",
    // 播放歌曲id列表
    idList: [],
    // 当前播放时间
    currentTime: '00:00',
    // 总播放时间
    duration: '03:30',
    // 进度条当前移动值
    move: 0,
    // 进度条最大值
    max: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options.idlist)
    // 接收首页传递过来的音乐id
    var mid= options.id
    let idListStr = options.idlist
    let idList = idListStr.split(",")
    // 小程序中设置初始id使用  setData方法
    this.setData({
      musicId: mid,
      idList:idList
    })
    this.getMusicDetail()
    this.getLyric()
  },

  //获取歌曲详情
  getMusicDetail: function () {
     let mid = this.data.musicId
     wx.request({
      url: `http://music.163.com/api/song/detail/?id=1359595520&ids=[${mid}]`,
      success: (result) => {
        let musicdata = result.data.songs;
        // console.log(this.data.musicdata);
        let musicName = musicdata[0].name
        let coverUrl = musicdata[0].album.blurPicUrl
        this.setData({
          name: musicName,
          imgUrl: coverUrl
        })
      },
      fail: (res) => {
        console.log('歌曲请求失败!')
      },
    })
  },

  // 控制播放器播放暂停的方法
  setPlayStatu: function () {
    console.log(this.data.action.method);
    // this.data.action.method = !this.data.action.method;
    var playStatu = this.data.action.method
    // 如果为播放状态则暂停，暂停则播放
    if (playStatu == "play") {
      this.setData({
        action: {
          "method":"pause"
        }
      })
    } else {
      this.setData({
        action: {
          "method":"play"
        }
      })
    }
  },

  // 获取歌词
  getLyric: function() {
    // 拿到当前歌曲id
    const that = this;
    let musicId = this.data.musicId;
    let src = `http://music.163.com/api/song/lyric?os=pc&id=${musicId}&lv=-1&tv=-1`
    wx.request({
      url: src,
      success: (result) => {
        // 定义一个最终时间歌词列表
        const lyricTimeList = []
        if (result.data.hasOwnProperty('lrc') === true) {
          this.setData({
            hasLyc: true
          })
          // 最终目的：拿时间和歌词对应的数据
          let musicLyric = result.data.lrc.lyric;
          // 对歌词字符串进行拆分，形成一个数组
          let lyricList = musicLyric.split('\n');
          for (let i = 0; i < lyricList.length; i++) {
            // 设置匹配时间的正则表达式，剔除掉时间，只要歌词
            const reg = /\[\d{2}:\d{2}\.\d{2,3}\]/;
            let time = lyricList[i].match(reg);
            // 去掉为空的情况
            if (time != null) {
              // 把每一个字符串中的时间替换为空字符串,得到歌词
              let lyc = lyricList[i].replace(reg, "");
              // 处理时间，把时间变为秒数
              // 用slice方法提取某个字符串一部分开始1 -1倒数位置
              // 去掉两边中括号得到时间纯数字
              let timeStr = time[0].slice(1,-1)
              // 得到01:09.500之后去掉 : 号
              let timeList = timeStr.split(':')
              // 把得到的["01", "14.750"]时间数组算成总秒数
              // 通过parseFloat把字符串变成数字
              let newTime = parseFloat(timeList[0])*60 + parseFloat(timeList[1])
              lyricTimeList.push([newTime,lyc])
              that.setData({
                lycTimeList:lyricTimeList
              })
            }
          }
        } else {
          return this.setData({
            hasLyc: false
          })
        }
      },
      fail: (res) => {
        console.log('获取歌词失败！')
      }
    })
  },

  // 随着播放器播放时间的改变，实时触发这个函数
  timeChange: function(time) {
    // 当前播放时间
    const atTime = time.detail.currentTime
    const timeList = this.data.lycTimeList
    for (let i = 0; i < timeList.length-1; i++) {
      if(timeList[i][0]<atTime&&atTime<timeList[i+1][0]) {
        // console.log(timeList[i][1])
        this.setData({
          index:i
        })
      }
      let index = this.data.index;
      if(index>5) {
        this.setData({
          top: (index-5)*33
        })
      }
    }

    // 进度条时间数据更新
    // 拿到当前时长
    // 拿到总时长
    let duration_time = time.detail.duration
    // 求分钟秒数以及格式化处理
    let play_m = Math.floor(atTime/60)
    let play_s = Math.floor(atTime%60)
    // 补零
    if(play_m <10 ) {
      play_m = "0"+ play_m
    }
    if(play_s < 10){
      play_s= "0" + play_s
    }
    let currentTime = play_m + ":" + play_s

    let sum_m = Math.floor(duration_time/60)
    let sum_s = Math.floor(duration_time%60)
    // 补零
    if(sum_m<10) {
      sum_m = "0"+ sum_m
    }
    if(sum_s<10){
      sum_s="0"+sum_s
    }
    let duration = sum_m+":"+sum_s

    this.setData({
      currentTime:currentTime,
      duration:duration,
      max:duration_time,
      move:atTime
    })
  },

  // 拖动进度条改变当前播放进度
  sliderChange:function(val){
    // console.log(val)
    // 修改当前播放时间
    this.setData({
      action:{
        method: 'setCurrentTime',
        data: val.detail.value
      }
    })
    // 更新播放状态
    this.setData({
      action: {
        method: 'play'
      }
    })
  },

  //改变播放模式的方法
  changeMode: function() {
    if(this.data.mode === 'loop'){
      this.setData({
        mode: "single"
      })
    } else {
      this.setData({
        mode: "loop"
      })
    }
  },

  // 音频播放结束触发的方法
  playEnded: function(){
    // single 单曲 loop 循环
    if(this.data.mode === 'single'){
      // id 改变，代表播放歌曲的改变,单曲循环歌曲id不变，设置回原本的id即可
      this.setData({
        musicId: this.data.musicId
      })
      // 刷新播放状态
      this.setData({
        action: {
          method: "play"
        }
      })
    }else{
      this.nextSong()
    }
  },

  // 切换下一首歌曲
  nextSong:function(){
    let id = this.data.musicId
    let idlist = this.data.idList
    let index = -1
    // 当前播放歌曲下标
    // let index = idlist.findIndex(item => {
    //   item = id
    // })
    for(let i = 0; i < idlist.length; i++) {
      if(id==idlist[i]) {
        index = i
        break
      }
    }
    // 当前播放歌曲的下标=播放列表歌曲最后一首的话，让其跳到第一首歌
    if(index==idlist.length-1){
      this.setData({
        musicId: idlist[0]
      })
    } else {
      // 否则正常播放，下一首
      this.setData({
        musicId: idlist[index+1]
      })
    }
    this.setData({
      action: {
        method: "play"
      }
    })
    this.getMusicDetail()
    this.getLyric()
  },

  // 上一首
  prevSong:function(){
    let id = this.data.musicId
    let idlist = this.data.idList
    let index = -1
    // 当前播放歌曲下标
    // let index = idlist.findIndex(item => {
    //   item = id
    // })
    for(let i = 0; i < idlist.length; i++) {
      if(id==idlist[i]) {
        index = i
        break
      }
    }
    // 当前播放歌曲的下标=播放列表第一首的话，让其跳到最后一首
    if(index==0){
      this.setData({
        musicId: idlist[idlist.length-1]
      })
    } else {
      // 否则正常播放，上一首
      this.setData({
        musicId: idlist[index-1]
      })
    }
    this.setData({
      action: {
        method: "play"
      }
    })
    this.getMusicDetail()
    this.getLyric()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})