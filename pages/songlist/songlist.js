// pages/songlist/songlist.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    background: ['http://p1.music.126.net/iJwGGrjRh_jMyjBhGF-3oA==/109951165986610083.jpg?imageView&quality=89', 'http://p1.music.126.net/i5lLmRLoC9-Be778LFObzQ==/109951165986604302.jpg?imageView&quality=89', 'http://p1.music.126.net/ZG156k20tQaWtav9SNNH5g==/109951165986602926.jpg?imageView&quality=89','http://p1.music.126.net/Vo49ZkCxKpcrBSQsu01N4A==/109951165986612523.jpg?imageView&quality=89','http://p1.music.126.net/n5WKofX4D4mXnbv7KNc3nw==/109951165987535730.jpg?imageView&quality=89'],
    // 歌曲数据列表
    songlist: [],
    // 搜索框输入的值
    word: "",
    // 歌手名字
    artistName: '',
    // 歌曲id列表
    idList: [],
    // 歌曲封面
    imgurl: [],
    // 歌曲请求条数
    musicLimit: 6
  },

  play: function (events) {
    var musicId = events.currentTarget.dataset.id
    // console.log(events.currentTarget.dataset.id);
    // console.log(this.data.idList)
    const idlist = this.data.idList
    wx.navigateTo({
      url: `/pages/play/play?id=${musicId}&idlist=${idlist}`,
    })
  },

  // 搜索输入框值发生改变触发这个函数
  keyWordChange: function(keyWord) {
    this.setData({
      word:keyWord.detail.value
    })
  },

  // 点击搜索
  search: function() {
    const sword = this.data.word;
    const musicLimit = this.data.musicLimit
    // 定义歌曲id数组
    const idList = [];
    wx.request({
      url: `http://music.163.com/api/search/get?s=${sword}&type=1&limit=${musicLimit}`,
      success: (result) => {
        const songList = result.data.result.songs
        this.setData({
          songlist: songList,
        })
        songList.map((item) => {
          return idList.push(item.id)
        })
        this.setData({
          idList:idList
        })
        // 每次搜索请求，都清空一下之前的数组，防止还是显示之前的老数组
        this.setData({
          imgurl:[]
        })
        // 调用找歌曲封面的方法
        this.getMusicImg(idList, 0, idList.length)
      },
      fail: (res) => {
        console.log('搜索失败!')
      },
    })
  },

  // 通过id获取歌曲封面的方法
  // id数组, 每次递归的下标, 结束下标
  getMusicImg:function(list,i,length) {
    let imgurl = this.data.imgurl
    const that = this
    let url = `http://music.163.com/api/song/detail/?id=
    1359595520&ids=[${list[i]}]`
    wx.request({
      url: url,
      success: (result) => {
        const img = result.data.songs[0].album.blurPicUrl
        imgurl.push(img)
        this.setData({
          imgurl:imgurl
        })
        // 跳出递归
        if(++i<length) {
          that.getMusicImg(list,i,length)
        }
      },
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    console.log('加载更多')
    let musicLimit = this.data.musicLimit
    let word = this.data.word
    // let that = this
    if(word!="") {
      let url = `http://music.163.com/api/search/get?s=${word}&type=1&limit=${musicLimit}`
      musicLimit+=2
      this.setData({
        musicLimit: musicLimit
      })
      // 定义歌曲id数组
      const idList = [];
      wx.showLoading({
        title: '歌曲加载中'
      })
      wx.request({ 
        url: url,
        success: (result) => {
          const songList = result.data.result.songs
          this.setData({
            songlist: songList
          })
          songList.map((item) => {
            return idList.push(item.id)
          })
          this.setData({
            idList:idList
          })
          // 每次搜索请求，都清空一下之前的数组，防止还是显示之前的老数组
          this.setData({
            imgurl:[]
          })
          // 调用找歌曲封面的方法
          this.getMusicImg(idList, 0, idList.length)
          setTimeout(() => {
            wx.hideLoading()
          }, 2000)
        }, 
        fail: (res) => {
          console.log('歌曲添加失败!')
        },
      })
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})