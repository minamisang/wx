// pages/find/find.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 推荐歌单列表
    hotlist:[],
    // 推荐新歌曲列表
    newSong: [],
    // 热单数据条数
    sum: 10,
    // 热曲数据条数
    limit: 10
  },

  // 点击加载更多热门推荐歌单
  showMore:function(){
    let num = this.data.sum
    num+=3
    this.setData({
      sum:num
    })
    this.gethotList(num)
  },

  // 点击加载更多热门推荐歌曲
  showMore1:function() {
    let limit = this.data.limit
    limit+=3
    this.setData({
      limit:limit
    })
    this.getNewSong(limit)
  },

  // 获取热门推荐歌单
  gethotList:function(sum) {
    wx.showLoading({
      title: '加载中',    
    })
    // 获取热门推荐歌单
    let url = `https://nicemusic-api.lxhcool.cn/personalized?limit=${sum}`
    wx.request({
      url: url,
      success: (result) => {
        let data = result.data.result
        this.setData({
          hotlist: data
        })
      },
      fail: (res) => {
        console.log('获取热门推荐歌单失败!')
      },
    })
    wx.hideLoading()
  },

  // 获取热门推荐歌曲
  getNewSong:function(limit) {
    wx.showLoading({
      title: '加载中',    
    })
    // 获取热门推荐歌曲
    let url = `https://nicemusic-api.lxhcool.cn/personalized/newsong?limit=${limit}`
    wx.request({
      url: url,
      success: (result) => {
        console.log(result)
        let data = result.data.result
        this.setData({
          newSong: data
        })
      },
    })
    wx.hideLoading()
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let limit = this.data.limit
    let sum = this.data.sum
    this.getNewSong(limit)
    this.gethotList(sum)
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})